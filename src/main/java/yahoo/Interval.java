package yahoo;

import javax.security.auth.callback.LanguageCallback;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class Interval {

	private String fromMonth = "";
	private String fromDay = "";
	private String fromYear = "";
	
	private String toMonth ="";
	private String toDay = "";
	private String toYear = "";
	
	public Interval(LocalDate fromDate, LocalDate toDate)
	{
		
		setFromMonth(Integer.toString(fromDate.getMonthValue()));
		setFromDay(Integer.toString(fromDate.getDayOfMonth()));
		setFromYear(Integer.toString(fromDate.getYear()));
		
		if( toDate!=null )
		{

			setToMonth(Integer.toString(toDate.getMonthValue()));
			setToDay(Integer.toString(toDate.getDayOfMonth()));
			setToYear(Integer.toString(toDate.getYear()));
		}
		else
		{
	    	setEndofIntervalV2();
		}
	}
	public Interval(SIDate fromDate, SIDate toDate)
	{

		setFromMonth(fromDate.getMonth());
		setFromDay(fromDate.getDay());
		setFromYear(fromDate.getYear());

		if( toDate!=null )
		{
			setToMonth(toDate.getMonth());
			setToDay(toDate.getDay());
			setToYear(toDate.getYear());
		}
		else
		{
	    	setEndofInterval();
		}
	}


	private void setEndofIntervalV2() {

		Date date = new Date();
		LocalDate date_today = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		setFromYear( Integer.toString(date_today.getYear()) );
		setFromMonth( Integer.toString( date_today.getMonthValue()) );
		setFromDay( Integer.toString( date_today.getDayOfMonth()) );
	}

	private void setEndofInterval() {
		Calendar date_today = Calendar.getInstance();
		date_today.setLenient(true);
		setFromYear( Integer.toString(date_today.get(Calendar.YEAR)) );
		setFromMonth( Integer.toString( date_today.get(Calendar.MONTH)) );
		setFromDay( Integer.toString( date_today.get(Calendar.DATE)) );
	}

	
	public String getFromDay()
	{
		return fromDay;
	}
	
	public void setFromDay(String fromDay)
	{
		this.fromDay = fromDay;
	}

	public String getFromMonth()
	{
		return fromMonth;
	}
	
	public void setFromMonth(String fromMonth)
	{
		this.fromMonth = fromMonth;
	}
	
	public String getFromYear()
	{
		return fromYear;
	}
	
	public void setFromYear(String fromYear)
	{
		this.fromYear = fromYear;
	}
	
	public String getToDay()
	{
		return toDay;
	}
	
	public void setToDay(String toDay)
	{
		this.toDay = toDay;
	}
	
	public String getToMonth()
	{
		return toMonth;
	}
	
	public void setToMonth(String toMonth)
	{
		this.toMonth = toMonth;
	}
	
	public String getToYear()
	{
		return toYear;
	}
	
	public void setToYear(String toYear)
	{
		this.toYear = toYear;
	}
	
	public SIDate getFromDate()
	{
		SIDate fromDate = new SIDate(getFromMonth(), getFromDay(), getFromYear());
		return fromDate;
	}
	
	public SIDate getToDate()
	{
		SIDate toDate = new SIDate(getToMonth(), getToDay(), getToYear());
		return toDate; 
	}
	
}

package yahoo;

import java.time.LocalDate;
import java.util.Calendar;

public class YahooStockData
{
	   private String stockName;
	    private LocalDate date;
	    private LocalDate locDate;
	    private String open;
	    private String high;
	    private String low;
	    private String close;
	    private String volume;
	    private String adjClose;

	    private String dateAsString;
	    
	    public String getDateAsString() {
			return dateAsString;
		}

		public void setDateAsString(String dateAsString) {
			this.dateAsString = dateAsString;
		}

		public String toString()
	    {
	     String stockDataAsString = "StockInfo = stockName: " +stockName + " date: " + date.getYear() + " " + date.getMonth() + " " + date.getDayOfWeek() + " locDate: " + locDate + " open: " + open + " high: " + high + " low: "+low +" close: " +close+" volume: " + volume + " adjClose: " + adjClose;
	     //cal.get(Calendar.YEAR) + " " + cal.get(Calendar.MONTH) + " " + cal.get(Calendar.DATE)	
	     return stockDataAsString;
	    }
	    
	    public YahooStockData() 
	    { 

	        // set Date portion to January 1, 1970
//	        date.set( Calendar.YEAR, 1970 );
//	        date.set( Calendar.MONTH, Calendar.JANUARY );
//	        date.set( Calendar.DATE, 1 );
//
//	        date.set( Calendar.HOUR_OF_DAY, 0 );
//	        date.set( Calendar.MINUTE, 0 );
//	        date.set( Calendar.SECOND, 0 );
//	        date.set( Calendar.MILLISECOND, 0 );

	    }
	    
		/**
		 * @return Returns the close.
		 */
		public String getClose() {
			return close;
		}
		/**
		 * @param close The close to set.
		 */
		public void setClose(String close) {
			if(close.equals("null"))close="0";
			this.close = close;
		}
		/**
		 * @return Returns the date.
		 */
		public LocalDate getDate() {
			return date;
		}
		/**
		 */
		public void setDate(int year, int month, int day) {
		    
			
			
		    date = LocalDate.of(year, month, day);
		}


		public void setLocDate(String dateAsString){

			this.locDate = LocalDate.parse(dateAsString);



		}

		public LocalDate getLocDate(){

			return locDate;

		}
		
		public void setDate(String token, String separator)
		{
			//Incomming date format from yahoo 2007-06-25 
			
			String[] dateToken = token.split(separator);
			if(separator.equals("/"))
			{
				
				setDate( Integer.parseInt(dateToken[2]), conVertMonthToInt( dateToken[0] ), Integer.parseInt(dateToken[1]) );
			}
			else
			{
				//setDate( Integer.parseInt(dateToken[0]), conVertMonthToInt( dateToken[1] ), Integer.parseInt(dateToken[2]) );
				setDate( Integer.parseInt(dateToken[0]), Integer.parseInt( dateToken[1] ), Integer.parseInt(dateToken[2]) );
				//setDate(dateToken[0], dateToken[1], dateToken[0]);
				dateAsString = dateToken[0] + dateToken[1] + dateToken[2];
			}
		}

		public void setDate(String token)
		{
			
			setDate(token, "-");
		}
		
		
		public void setDate(String year, String month, String day)
		{
			setDate( Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day) );
		}
		
		/**
		 * @return Returns the high.
		 */
		public String getHigh() {

            return high;
		}
		/**
		 * @param high The high to set.
		 */
		public void setHigh(String high) {
            if(high.equals("null"))high="0";
            this.high = high;
		}
		/**
		 * @return Returns the low.
		 */
		public String getLow() {
            return low;
		}
		/**
		 * @param low The low to set.
		 */
		public void setLow(String low) {
            if(low.equals("null"))low="0";
            this.low = low;
		}
		/**
		 * @return Returns the open.
		 */
		public String getOpen() {
			return open;
		}
		/**
		 * @param open The open to set.
		 */
		public void setOpen(String open) {
		    if(open.equals("null"))open="0";
			this.open = open;
		}
		/**
		 * @return Returns the stockName.
		 */
		public String getStockName() {
			return stockName;
		}
		/**
		 * @param stockName The stockName to set.
		 */
		public void setStockName(String stockName) {
			this.stockName = stockName;
		}
		/**
		 * @return Returns the volume.
		 */
		public String getVolume() {
            return volume;
		}
		/**
		 * @param volume The volume to set.
		 */
		public void setVolume(String volume) {
            if(volume.equals("null"))volume="0";
            this.volume = volume;
		}
		
		private int conVertMonthToInt( String month )
		{
			
		    if(month.compareTo("Jan") == 0)
			month = "00";
		    else if(month.compareTo("Feb") == 0)
			month = "01";
		    else if(month.compareTo("Mar") == 0)
			month = "02";
		    else if(month.compareTo("Apr") == 0)
			month = "03";
		    else if(month.compareTo("May") == 0)
			month = "04";
		    else if(month.compareTo("Jun") == 0)
			month = "05";
		    else if(month.compareTo("Jul") == 0)
			month = "06";
		    else if(month.compareTo("Aug") == 0)
			month = "07";
		    else if(month.compareTo("Sep") == 0)
			month = "08";
		    else if(month.compareTo("Oct") == 0)
			month = "09";
		    else if(month.compareTo("Nov") == 0)
			month = "10";
		    else if(month.compareTo("Dec") == 0)
			month = "11";		    

		    int intMonth = Integer.parseInt(month);
		    
		    return intMonth;
		}

		public String getAdjClose() {
			return adjClose;
		}

		public void setAdjClose(String adjClose) {
            if(adjClose.equals("null"))adjClose="0";
            this.adjClose = adjClose;
		}	
}

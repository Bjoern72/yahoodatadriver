package yahoo;

//https://www.epochconverter.com/
//https://stackoverflow.com/questions/44030983/yahoo-finance-url-not-working/44050039#44050039
//https://stackoverflow.com/questions/44044263/yahoo-finance-historical-data-downloader-url-is-not-working


//import loopback.Stock;
//import com.mashape.unirest.http.HttpResponse;
//import com.mashape.unirest.http.Unirest;
//import com.mashape.unirest.http.exceptions.UnirestException;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;


import java.io.*;
import java.net.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YahooDataDriver //implements StockDataDriver
{
	Logger log = Logger.getLogger(this.getClass().getName());
	static ArrayList intervals = new ArrayList();
	static ArrayList<YahooStockData> stockDataLst = new ArrayList<YahooStockData>();

	public ArrayList<YahooStockData> getData(String symbol, LocalDate fromDate, LocalDate toDate)
	{

		ArrayList<YahooStockData> yahooDataList = null;
		try
		{
			yahooDataList = dwLoadYahooData(symbol, fromDate, toDate);

		}
		catch (Exception e)
		{
			// TODO: handle exception
			log.log(Level.INFO, e.getMessage(), e);
		}
//		try {
//			Unirest.shutdown();
//		} catch (IOException e) {
//			log.log(Level.INFO, e.getMessage(), e);
//		}
		return yahooDataList;
	}

	private String getQuery_V7(String symbol, Interval interval, String crump) throws IOException {
//https://stackoverflow.com/questions/22990067/how-to-extract-epoch-from-localdate-and-localdatetime

    	LocalDate fromDate = LocalDate.of(new Integer(interval.getFromYear()),new Integer(interval.getFromMonth()), new Integer (interval.getFromDay()));

		ZoneId zoneId = ZoneId.systemDefault(); // or: ZoneId.of("Europe/Oslo");
		long epochPeriod1 = fromDate.atStartOfDay(zoneId).toEpochSecond();
		System.out.println(epochPeriod1);
		System.out.println("LocalDate from: " + fromDate);
		LocalDate toDate = LocalDate.of(new Integer(interval.getToYear()),new Integer(interval.getToMonth()), new Integer (interval.getToDay()));
		long epochPeriod12 = toDate.atStartOfDay(zoneId).toEpochSecond();

		System.out.println(epochPeriod12);
		System.out.println("LocalDate to: " + toDate);

		String basic = "https://query1.finance.yahoo.com/v7/finance/download/" + symbol + "?"+ "period1" + "=" +epochPeriod1+ "&"+ "period2" +"="+epochPeriod12+ "&" + "interval" + "="+ "1d" + "&"+ "events"+"="+ "history"+"&"+"crumb"+"="+ crump;


		System.out.println(basic);

		return basic;
	}


	private ArrayList<YahooStockData> dwLoadYahooData(String symbol, LocalDate fromDate, LocalDate toDate) throws IOException, UnirestException {

		CookieAndCrumb cookieAndCrumb = getCrumb();
		//CookieAndCrumb cookieAndCrumb = getCrumbV2();

		System.out.println("SI fromDate: " + fromDate);
		System.out.println("SI toDate: " + toDate);
		ArrayList<YahooStockData> yahooDataList = new ArrayList<>();
		URL url = new URL(getQuery_V7(symbol, new Interval(fromDate, toDate), cookieAndCrumb.getCrumb()));

		URLConnection con = url.openConnection();


		con.setRequestProperty("cookie", cookieAndCrumb.getCookie());

//		con.setInstanceFollowRedirects(false);  //you still need to handle redirect manully.
//		HttpURLConnection.setFollowRedirects(false);


		BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		LineNumberReader lr = new LineNumberReader(br);

		String line = "";

		int lineCount = 0;
		lr.setLineNumber(lineCount);
		while( ( line = lr.readLine()) != null )
		{
			if(lr.getLineNumber()<=2) //TODO for some reason the first line contains nulls and the date is the day before the interval should beging
			{
				continue;
			}
			else
			{
				String[] tokenData = line.split(",");
				YahooStockData yahooData = new YahooStockData();

				yahooData.setStockName(symbol);
				yahooData.setDate(tokenData[0]);
				yahooData.setLocDate(tokenData[0]);
				yahooData.setOpen(tokenData[1]);
				yahooData.setHigh(tokenData[2]);
				yahooData.setLow(tokenData[3]);
				yahooData.setClose(tokenData[4]);
				yahooData.setVolume(tokenData[5]);
				yahooData.setAdjClose(tokenData[6]);
				//System.out.println(yahooData);
				//log.info("prit: "+yahooData);
				yahooDataList.add(yahooData);

			}
			lineCount++;
			lr.setLineNumber(lineCount);
		}
		System.out.println(yahooDataList);
		log.info("yahooDataList");
		return yahooDataList;
	}


    private CookieAndCrumb cookieAndCrumb = new CookieAndCrumb();


	List<String> header = null;
	private CookieAndCrumb getCrumb() throws IOException, UnirestException {

		Date date = new Date();
		LocalDate now = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		JSONParser parser = new JSONParser();
		CookieAndCrumb cookieAndCrumb = new CookieAndCrumb();
		Object obj = null;
		JSONObject jsonObject;

		try {

			InputStream inputStream = getClass().getResourceAsStream("/yahoo_cookie_crumb.json");
			//File file = new File(url.getPath());

//			obj = parser.parse(new FileReader(
//                    "C:\\ws_git\\docker-spring-boot\\yahoo_cookie_crumb.json"));
			obj = parser.parse(new InputStreamReader(inputStream));

			jsonObject = (JSONObject) obj;
			long lastUpdated = (long) jsonObject.get("date");

			//long lastUpdated = Long.parseLong(lastUpdatedAsString);

			// unix time to java.time.LocalDateTime (since Java 8)
			LocalDate lastUpdDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastUpdated), ZoneId.systemDefault()).toLocalDate();

			if(now.minusMonths(1).isAfter(lastUpdDate)){

				HttpResponse<String> jsonResponse = Unirest.get("https://finance.yahoo.com/quote/GOOG?").asString();
				if(null==header){

					header = jsonResponse.getHeaders().get("set-cookie");
				}

				cookieAndCrumb.setCookie(header.get(0));

				Pattern crumbPattern = Pattern.compile(".*\"CrumbStore\":\\{\"crumb\":\"([^\"]+)\"\\}.*", Pattern.DOTALL);

				String bodyAsString = jsonResponse.getBody();

				Matcher matcher = crumbPattern.matcher(bodyAsString);

				String crumb = null;

				if (matcher.matches()) {
					crumb = matcher.group(1);
				}

				//cookieAndCrumb.setCrumb(unescapeJavaString(crumb));

				jsonObject.put("date", now.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli());
				jsonObject.put("cookie", header.get(0));
				jsonObject.put("crumb", crumb);

				// try-with-resources statement based on post comment below :)
//				try (FileWriter file = new FileWriter(url.getPath())) {
//					file.write(jsonObject.toJSONString());
//					System.out.println("Successfully Copied JSON Object to File...");
//					System.out.println("\nJSON Object: " + obj);
//				}

				FileOutputStream outputStream = new FileOutputStream("/yahoo_cookie_crumb.json");
				byte[] strToBytes = jsonObject.toJSONString().getBytes();
				outputStream.write(strToBytes);

				outputStream.close();

				Unirest.config()
						.reset()
						.connectTimeout(5000);
			}
			else{

				cookieAndCrumb.setCookie( (String) jsonObject.get("cookie"));
				cookieAndCrumb.setCrumb( (String) jsonObject.get("crumb"));

			}

		} catch (ParseException e) {
			e.printStackTrace();
		}




//		// try-with-resources statement based on post comment below :)
//		try (FileWriter file = new FileWriter("/Users/<username>/Documents/file1.txt")) {
//			file.write(obj.toJSONString());
//			System.out.println("Successfully Copied JSON Object to File...");
//			System.out.println("\nJSON Object: " + obj);
//		}





//        Map<String, List<String>> headers = Unirest.get("https://finance.yahoo.com/quote/GOOG?")
//    .getHeaders();
//		Set<Map.Entry<String, List<String>>> headers = Unirest.get("https://finance.yahoo.com/quote/GOOG?")
//				.getHeaders().entrySet();



		//String headerSim = "B=4jcqlrde9882v&b=3&s=bp; expires=Fri, 20-Mar-2020 23:38:08 GMT; path=/; domain=.yahoo.com";
		//String headerextract = header.get(0);

		//cookieAndCrumb.setCookie(headerextract);

		//cookieAndCrumb.setCookie(headerSim);

		//Pattern crumbPattern = Pattern.compile(".*\"CrumbStore\":\\{\"crumb\":\"([^\"]+)\"\\}.*", Pattern.DOTALL);



//		InputStream inStream = jsonResponse.getRawBody();
//		InputStreamReader irdr = new InputStreamReader(inStream);
//		BufferedReader rsv = new BufferedReader(irdr);
//
//		String line = null;
//		while (crumb == null && (line = rsv.readLine()) != null) {
//			Matcher matcher = crumbPattern.matcher(line);
//			if (matcher.matches()) {
//				crumb = matcher.group(1);
//			}
//		}
//		rsv.close();



		return cookieAndCrumb;

	}

	public String unescapeJavaString(String st) {

		StringBuilder sb = new StringBuilder(st.length());

		for (int i = 0; i < st.length(); i++) {
			char ch = st.charAt(i);
			if (ch == '\\') {
				char nextChar = (i == st.length() - 1) ? '\\' : st
						.charAt(i + 1);
				// Octal escape?
				if (nextChar >= '0' && nextChar <= '7') {
					String code = "" + nextChar;
					i++;
					if ((i < st.length() - 1) && st.charAt(i + 1) >= '0'
							&& st.charAt(i + 1) <= '7') {
						code += st.charAt(i + 1);
						i++;
						if ((i < st.length() - 1) && st.charAt(i + 1) >= '0'
								&& st.charAt(i + 1) <= '7') {
							code += st.charAt(i + 1);
							i++;
						}
					}
					sb.append((char) Integer.parseInt(code, 8));
					continue;
				}
				switch (nextChar) {
					case '\\':
						ch = '\\';
						break;
					case 'b':
						ch = '\b';
						break;
					case 'f':
						ch = '\f';
						break;
					case 'n':
						ch = '\n';
						break;
					case 'r':
						ch = '\r';
						break;
					case 't':
						ch = '\t';
						break;
					case '\"':
						ch = '\"';
						break;
					case '\'':
						ch = '\'';
						break;
					// Hex Unicode: u????
					case 'u':
						if (i >= st.length() - 5) {
							ch = 'u';
							break;
						}
						int code = Integer.parseInt(
								"" + st.charAt(i + 2) + st.charAt(i + 3)
										+ st.charAt(i + 4) + st.charAt(i + 5), 16);
						sb.append(Character.toChars(code));
						i += 5;
						continue;
				}
				i++;
			}
			sb.append(ch);
		}
		return sb.toString();
	}

	public static void main(String[] args) throws IOException {

		YahooDataDriver yahooDriver = new YahooDataDriver();
		LocalDate fromDate = LocalDate.of(2017, 6, 1);
		LocalDate toDate = LocalDate.of(2017, 12,27);

		//String stockName 	= "DSVENA.CO";
		//String stockName 	= "MAERSK-B.CO";
		String stockName 	= "CCI";
		ArrayList yahooData = yahooDriver.getData(stockName, fromDate, toDate);
		
		for(int i=0; i!= yahooData.size();i++)
		{
			System.out.println(yahooData.get(i));
		}
	}
}

package yahoo;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CookieAndCrumb {

    private LocalDate lastUpdateDate;
    private String cookie;
    private String crumb;

}

package generics.comparison;

public class TestComparable {

    public static void main(String[] args) {

        Integer int0 = 0;
        Integer int1 = 1;
        assert int0.compareTo(int1) < 0;

        String str0 = "zero";
        String str1 = "one";
        assert str0.compareTo(str1) > 0;

    }

}

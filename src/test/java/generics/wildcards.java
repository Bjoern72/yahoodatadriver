package generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class wildcards {

    public static void main(String[] args) {
        List<Number> nums = new ArrayList<>();
        nums.add(2);
        nums.add(3.14);
        System.out.println(nums.toString());
        assert nums.toString().equals("[2, 3.14]");

        List<Integer> ints = Arrays.asList(1, 2);

        //List<Number> nums2 = ints;


    }
}

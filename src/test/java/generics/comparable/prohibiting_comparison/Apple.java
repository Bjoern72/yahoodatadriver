package generics.comparable.prohibiting_comparison;

class Apple extends Fruit implements Comparable<Apple>{

    public Apple(int size) {
        super("Apple", size);
    }

    @Override
    public int compareTo(Apple a) {
        return super.compareTo(a);

    };

}

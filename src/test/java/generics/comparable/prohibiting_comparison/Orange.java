package generics.comparable.prohibiting_comparison;

import com.sun.org.apache.xpath.internal.operations.Or;

class Orange extends Fruit implements Comparable<Orange>{

    public Orange(int size) {
        super("Orange", size);
    }

    @Override
    public int compareTo(Orange o) {
        return super.compareTo(o);

    }
}

package generics.comparable.prohibiting_comparison;

public abstract class Fruit /*implements Comparable<Fruit>*/{
    protected String name;
    protected int size;

    public Fruit(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public boolean equals(Object o){
        if(o instanceof Fruit) {
            Fruit that = (Fruit)o;
            return this.name.equals(that.name) && this.size == that.size;
        } return false;
    }

    public int hash(){
        return name.hashCode()*29 + size;
    }

    /*@Override*/
    protected int compareTo(Fruit that) {
        return this.size < that.size ? -1 : this.size == that.size ? 0 : 1;
    }


}

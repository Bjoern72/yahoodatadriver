package generics.comparable.permitting_comparison;

class
Apple extends Fruit {

    public Apple(int size) {
        super("Apple", size);
    }
}

package generics.comparable.permitting_comparison;

class Orange extends Fruit {

    public Orange(int size) {
        super("Orange", size);
    }
}

package generics;

import java.util.ArrayList;
import java.util.List;

public class Lists {

//    public static <T> List<T> toList(T[] arr){
//
//        List<T> list = new ArrayList<>();
//        for(T elt : arr){
//            list.add(elt);
//            return list;
//        }
//        return list;
//    }

    public static <T> List<T> toList(T... arr){

        List<T> list = new ArrayList<>();
        for(T elt : arr){
            list.add(elt);
            return list;
        }
        return list;
    }

    public static void main(String[] args) {
        List<Integer> ints = Lists.toList(1,2,3);
        List<String> words = Lists.toList("hello", "world");

    }
}

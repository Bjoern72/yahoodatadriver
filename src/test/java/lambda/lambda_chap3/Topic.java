package lambda.lambda_chap3;

public enum Topic {

    HISTORY, PROGRAMMING, MEDICINE, COMPUTING, FICTION;
}

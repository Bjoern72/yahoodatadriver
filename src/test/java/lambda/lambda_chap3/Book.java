package lambda.lambda_chap3;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Book {


    public Book(String title, List<String> authors, int[] pageCounts, Year pubDate, double height, Topic topic) {
        this.title = title;
        this.authors = authors;
        this.pageCounts = pageCounts;
        this.pubDate = pubDate;
        this.height = height;
        this.topic = topic;
    }

    public static void main(String[] args) {

        Book nails = new Book("Fundamentals of Chinese Fingernail Image",
                Arrays.asList("Li", "Fu", "Li"),
                new int[]{256},
                Year.of(2014),
                25.2,
                Topic.MEDICINE);

        Book dragon = new Book("Compilers: Principles, Techniques and Tools",
                Arrays.asList("Aho", "Lam", "Sethi", "Ullman"),
                new int[] {1009},
                Year.of(2014),
                23.6,
                Topic.COMPUTING);

        Book voss = new Book("Voss",
                Arrays.asList("voss"),
                new int[]{478},
                Year.of(2014),
                19.8,
                Topic.FICTION);

        Book lotr = new Book("Lord of the Rings",
                Arrays.asList("Tolkien"),
                new int[]{531, 416, 624},
                Year.of(2014),
                23.0,
                Topic.FICTION);

        List<Book> library = Arrays.asList(nails, dragon, voss, lotr);


        Stream<Book> computingBooks = library.stream().filter(book -> book.getTopic() == Topic.COMPUTING);

        Stream<String> bookTitles = library.stream().map(Book::getTitle);

        //p.
        Path start = new File("startdir").toPath();
        Pattern pattern = Pattern.compile("[-+]?[0-9]*.?[0-9]+");
        PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("**/test*.txt");




    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public int[] getPageCounts() {
        return pageCounts;
    }

    public void setPageCounts(int[] pageCounts) {
        this.pageCounts = pageCounts;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Year getPubDate() {
        return pubDate;
    }

    public void setPubDate(Year pubDate) {
        this.pubDate = pubDate;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    private String title;
    private List<String> authors;
    private int[] pageCounts;
    private Year pubDate;
    private double height;
    private Topic topic;

}

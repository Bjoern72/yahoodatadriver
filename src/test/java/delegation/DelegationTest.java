package delegation;

import org.junit.jupiter.api.Test;

public class DelegationTest {

    @Test
    void delegationPatternTest() {

        Luggage luggage = new Luggage();

        FlightSegment flightSegment = new FlightSegment();


        try {
            flightSegment.checkLuggage(luggage);
        } catch (LuggageException e) {
            e.printStackTrace();
        }


    }
}

package delegation.solid.dip.withinjection;



public class Client {




    public static void main(String[] args) {
        IMessageService iMessageService = new SMS();
        Notification notification = new Notification(iMessageService);
        notification.promotionalNotification();
        iMessageService = new Email();
        notification.promotionalNotification(iMessageService);
    }




}

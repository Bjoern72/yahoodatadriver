package delegation.solid.dip.withinjection;

public interface IMessageService {

    public void sendMessage();

}

package delegation.solid.dip.withinjection;

public class SMS implements IMessageService {
    @Override
    public void sendMessage() {
        System.out.println("sends SMS..");
    }
}

package delegation.solid.dip.withinjection;

public class Email implements IMessageService {
    @Override
    public void sendMessage() {
        System.out.println("sends email..");
    }
}

package delegation.solid.dip.withinjection;

public class Notification {

    IMessageService iMessageService;

    public Notification(IMessageService iMessageService) {
        this.iMessageService = iMessageService;
    }

    public void promotionalNotification(){

        iMessageService.sendMessage();
    }

    public void promotionalNotification(IMessageService iMessageService){

        iMessageService.sendMessage();
    }

}

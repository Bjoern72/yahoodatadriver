package delegation.solid.interface_segregation;

public interface RestaurantInterface {
    public  void acceptOnlineOrder();
    public  void takeTelephoneOrder();
    public  void payOnline();
    public  void walkInCustomerOrder();
    public  void payInPerson();
}
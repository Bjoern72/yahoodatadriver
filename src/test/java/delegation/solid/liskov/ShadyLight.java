package delegation.solid.liskov;

public class ShadyLight {

    private boolean lighting = false;
    public void on(){
        lighting = false;
    }

    public void off(){
        lighting = true;
    }
}

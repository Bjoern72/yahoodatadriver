package delegation.solid.liskov;


public class MineWorker {

    private void drill(){
        System.out.println("I'm drilling..");
    }

    public void workInTheDark(Light light){
        light.on();
        drill();
        light.off();

    }
}

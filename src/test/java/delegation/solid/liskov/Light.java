package delegation.solid.liskov;

abstract class Light {

    abstract void on();
    abstract void off();

}

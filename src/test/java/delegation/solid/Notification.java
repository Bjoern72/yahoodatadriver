package delegation.solid;

import delegation.solid.Email;

public class Notification {

    private Email email;

    public Notification() {
        email = new Email();
    }

    public void promotionalNotification(){

        email.sendEmail();
    }
}

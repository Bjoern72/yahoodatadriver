package delegation;

/**
 * Instances of this class represent a piece of luggage.
 */
class Luggage {
    private float weight = 10;         // weight in kg
    //...
    /**
     * Return the weight of this piece of luggage in kg.
     */
    float getWeight() {
        return weight;
    } // getWeight()
} // class Luggage

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@Getter
@EqualsAndHashCode
abstract class StronglyTypedString {
    private final String stringValue;

    StronglyTypedString(@NonNull final String stringValue) {
        this.stringValue = stringValue;
    }
}
